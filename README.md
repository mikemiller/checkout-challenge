My solution for the [Checkout Kata Challenge](http://codekata.com/kata/kata09-back-to-the-checkout/).

How to run:

```sh
$ irb
$ require_relative 'checkout'
$ checkout = CheckOut.new
```


Tests

```sh
$ ruby checkout_test.rb
```
