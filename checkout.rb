require 'json'

class CheckOut
  attr_accessor :basket
  attr_accessor :price_list

  PATH  = File.join(File.dirname(__FILE__), 'rules.json')
  RULES = JSON.parse(File.read(PATH))

  def initialize
    @basket     = []
    @price_list = apply_rules_to_price_list
  end

  def apply_rules_to_price_list
    price_list = price_lookup
    RULES.each do |item, rule|
      price_list[item][:special_price]      = rule['special_price']
      price_list[item][:quantity_condition] = rule['quantity_condition']
    end
    price_list
  end

  def scan(item_name)
    basket << item_name
  end

  def total
    @total = 0
    counted = []
    basket.each do |item|
      next if counted.include? item
      quantity = basket.select { |prod| prod == item }.count
      item_promo = special_promotion(item, quantity)
      if item_promo
        calculate_promotion(item_promo, quantity)
      else
        @total += quantity * price_list[item][:unit_price]
      end
      counted << item
    end
    @total
  end

  private

  def price_lookup
    { 'A' => { unit_price: 50 },
      'B' => { unit_price: 30 },
      'C' => { unit_price: 20 },
      'D' => { unit_price: 15 } }
  end

  def special_promotion(item, quantity)
    item_promo = price_list[item]
    item_promo if item_promo[:special_price] && item_promo[:quantity_condition] && quantity >= item_promo[:quantity_condition]
  end

  def calculate_promotion(item_promo, quantity)
    remainder = quantity - item_promo[:quantity_condition]
    apply_promotion(item_promo)
    while remainder >= item_promo[:quantity_condition]
      remainder -= item_promo[:quantity_condition]
      apply_promotion(item_promo)
    end
    @total += remainder * item_promo[:unit_price]
  end

  def apply_promotion(item_promo)
    @total += item_promo[:quantity_condition] * item_promo[:special_price]
  end
end
